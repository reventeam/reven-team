﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class HUDScript : MonoBehaviour
{
		public Vector2 Position = new Vector2 (5, 5);
		public Vector2 Dimensions = new Vector2 (20, 70);
		public GUIStyle style;
		public Texture Background;
		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void Update ()
		{

		}

		void OnGUI ()
		{
				var x = (int)Extensions.PixelToPercent (Position.x, Screen.width);
				var y = (int)Extensions.PixelToPercent (Position.y, Screen.width);
				var width = (int)Extensions.PixelToPercent (Dimensions.x, Screen.width);
				var height = (int)Extensions.PixelToPercent (Dimensions.y, Screen.width);
				GUI.BeginGroup (new Rect (x, y, width, height), Background);
				GUI.Box (new Rect (3, 3, Dimensions.x, Dimensions.y), "");
				GUI.TextArea (new Rect (3, 3, Dimensions.x, Dimensions.y), Time.time.ToString ().Substring (0, 5), style);
				GUI.EndGroup ();
		}
}
