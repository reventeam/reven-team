﻿using UnityEngine;
using System.Collections;

public class AddAtraktorScript : MonoBehaviour
{
	public float time = 2.0F;
	public float timeStalych = 4.0F;
		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void Update ()
		{
				bool lpm = Input.GetMouseButtonDown (0);
				if (lpm) {
						GameObject atraktorKopia = GameObject.FindGameObjectWithTag ("Atraktor");
						var mousePos = Input.mousePosition;
						mousePos.z = 17f; // we want 2m away from the camera position
			
						var objectPos = Camera.main.ScreenToWorldPoint (mousePos);
						Vector2 projectResult = new Vector2();
						Vector3.Project (objectPos, projectResult);
						projectResult *= objectPos.magnitude;
			//tworzymy obiekt:
						var atraktor = Instantiate (atraktorKopia, objectPos, Quaternion.identity) as GameObject;
			atraktor.GetComponent<GravityPoint>().enabled = true;
			atraktor.GetComponent<SpriteRenderer>().enabled = true;
			  Destroy(atraktor, time);
			//Destroy(atraktorKopia,  timeStalych);
			//Destroy(atraktorKopia, time2) niszczone te juz POSTAWIONE w czasie time2
						atraktor.transform.parent = atraktorKopia.transform.parent;
						//atraktor.transform.position.z = 0;
						
						//atraktor.rigidbody2D.velocity.Scale(new Vector2(0,0));
						//atraktor.rigidbody2D.gravityScale = -2;
				}
		}
}

