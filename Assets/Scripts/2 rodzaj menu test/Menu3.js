﻿#pragma strict

var buttonStyle : GUIStyle;
 
function OnGUI()
{
    var groupWidth = 120;
    var groupHeight = 190;
     
    var screenWidth = Screen.width;
    var screenHeight = Screen.height;
     
    var groupX = ( screenWidth - groupWidth ) / 2  + 130;
    var groupY = ( screenHeight - groupHeight ) / 2  + 35;
     
    GUI.BeginGroup( Rect( groupX, groupY, groupWidth, groupHeight ) );
    GUI.Box( Rect( 0, 0, groupWidth, groupHeight ), "" );
     
    if ( GUI.Button( Rect( 10, 30, 100, 30 ), "Play", buttonStyle ) )
    {
       print ("you clicked the text button");
    }
    if ( GUI.Button( Rect( 10, 70, 100, 30 ), "About", buttonStyle ) )
    {
      Application.LoadLevel("projekt");
    }
    if ( GUI.Button( Rect( 10, 110, 100, 30 ), "Settings", buttonStyle ) )
    {
       print ("you cli3cked the text button");
    }
    if ( GUI.Button( Rect( 10, 150, 100, 30 ), "Exit", buttonStyle ) )
    {
       print ("you cli3cked the text button");
    }
     
    GUI.EndGroup();
}