﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class Pasek2 : MonoBehaviour {
	public GUIStyle GUIStyle = null;
	public GUIStyle GUIStyleThumb = null;
	public float InitialValue = 0.0F;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void OnGUI ()
	{
		int width = Screen.width;
		int height = Screen.height;
		int groupWidth = Extensions.PixelToPercent (5, width);  // szerokosc black-boxa, ok.
		int groupHeight = Extensions.PixelToPercent (22, width); //wysokosc black-boxa, ok.
		
		int groupX = Extensions.PixelToPercent (95, width); // rozmieszczenie black-boxa, ok.
		int groupY = Extensions.PixelToPercent (35, height); //rozmieszczenie black-boxa, ok.
		int buttonHeight = Extensions.PixelToPercent (10, height);
		int buttonWidth = Extensions.PixelToPercent (20, width);
		
		GUI.BeginGroup (new Rect (groupX,groupY, groupWidth, groupHeight));

		float WitdhSuwakaOdKrawedzi =  (0.35f * groupWidth);
		float HeightSuwakaOdKrawedzi = (0.07f * groupHeight);

		float WitdhSuwaka = (0.88f * groupWidth);
		float HeightSuwaka = (0.88f * groupHeight);



		GUI.Box (new Rect (0, 0, groupWidth, groupHeight), "");
		InitialValue = GUI.VerticalSlider(new Rect(WitdhSuwakaOdKrawedzi, HeightSuwakaOdKrawedzi, WitdhSuwaka, HeightSuwaka), InitialValue, 100.0F, 0.0F);
		//20,20 relatywnie liczone od 0. , tj. 50% witdh blackboxa, czyli witdh/height suwaka OdKrawedzi, ustawione %.

		// , szerokosc suwaka, wielkosc suwaka
		GUI.EndGroup ();
	}
}



