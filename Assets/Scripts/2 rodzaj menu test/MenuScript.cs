﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class MenuScript : MonoBehaviour
{

		public Texture2D PlayTexture = null;
		public Texture2D AboutTexture = null;
		public Texture2D SettingsTexture = null;
		public Texture2D HelpTexture = null;
		public GUIStyle GUIStyle = null;
		// Use this for initialization
		void Start ()
		{

		}


// Update is called once per frame
		void Update ()
		{
	
		}

		private void OnGUI ()
		{

				int width = Screen.width;
				int height = Screen.height;

		
		GUIStyle.fontSize = Extensions.PixelToPercent (4, width);

				int groupWidth = Extensions.PixelToPercent (35, width);
				int groupHeight = Extensions.PixelToPercent (22, width);
	
				int groupX = Extensions.PixelToPercent (55, width);
				int groupY = Extensions.PixelToPercent (45, height);
				int buttonHeight = Extensions.PixelToPercent (10, height);
				int buttonWidth = Extensions.PixelToPercent (35, width);

				GUI.BeginGroup (new Rect (groupX, groupY, groupWidth, groupHeight));
				bool play = GUI.Button (new Rect (1, 2, buttonWidth, buttonHeight), "play", GUIStyle);

				bool about = GUI.Button (new Rect (1, 2 + buttonHeight * 1, buttonWidth, buttonHeight), "about", GUIStyle);
				bool settings = GUI.Button (new Rect (1, 2 + buttonHeight * 2, buttonWidth, buttonHeight), "settings", GUIStyle);
				bool help = GUI.Button (new Rect (1, 2 + buttonHeight * 3, buttonWidth, buttonHeight), "help", GUIStyle);

				GUI.Box (new Rect (0, 0, groupWidth, groupHeight), "");
		            
				if (play) {
						print (Screen.GetResolution);
				}
				if (about) {
						Application.LoadLevel ("projekt");
				}
				if (settings) {
						print ("you clicked the text button");
				}
				if (help) {
						print ("you clicked the text button");
				}
		
				GUI.EndGroup ();
		}
}