﻿using UnityEngine;
using System.Collections;

public class GravityPoint : MonoBehaviour {

	public float maxGravDist = 100.0f;
	public float maxGravity = 15.0f;
	private const string atractorTag = "Attracted";
	GameObject[] atractors;
	
	void Start () {
		atractors = GameObject.FindGameObjectsWithTag(atractorTag);
	}
	
	void FixedUpdate () {
		foreach(GameObject atractor in atractors) {
			float dist = Vector3.Distance(atractor.transform.position, transform.position);
			if (dist <= maxGravDist) {
				Vector3 v = atractor.transform.position - transform.position;
				Vector2 vv = v.normalized * (1.0f - dist / maxGravDist) * maxGravity;
				atractor.rigidbody2D.AddForce(-vv);
			}
		}
	}
}
