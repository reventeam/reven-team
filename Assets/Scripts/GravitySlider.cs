﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class GravitySlider : MonoBehaviour
{
		public GUIStyle GUIStyle = null;
		public GUIStyle GUIStyleThumb = null;
		public float TotalSpeed = 5f;
		public float scale = 50f;
		const string atractorTag = "Attracted";
		float InitialAngle = 0.0F;
		float Angle = 0.0F;
		Vector2 speed;
		GameObject[] atractors;
		bool buttonPressed;

		void Start ()
		{
				atractors = GameObject.FindGameObjectsWithTag (atractorTag);
				speed = new Vector2 (TotalSpeed, TotalSpeed);
		}
		
		// Update is called once per frame
		void Update ()
		{
				if (Input.GetMouseButton (0)) {
						buttonPressed = true;

				} else {
						buttonPressed = false;
						InitialAngle = 0f;
				}
				Debug.Log (buttonPressed);
		}
		
		void FixedUpdate ()
		{
				
		}

		private void OnGUI ()
		{
				int width = Screen.width;
				int height = Screen.height;
				int groupWidth = Extensions.PixelToPercent (5, width);  // szerokosc black-boxa, 
				int groupHeight = Extensions.PixelToPercent (52, width); //wysokosc black-boxa, 
			
				int groupX = Extensions.PixelToPercent (95, width); // rozmieszczenie black-boxa,z prawej strony ekranu,ok.
				int groupY = Extensions.PixelToPercent (5, height); //rozmieszczenie black-boxa, od góry ekranu ile cm pusto
				int buttonHeight = Extensions.PixelToPercent (10, height);
				int buttonWidth = Extensions.PixelToPercent (20, width);
			
				GUI.BeginGroup (new Rect (groupX, groupY, groupWidth, groupHeight));
			
				float WitdhSuwakaOdKrawedzi = (0.35f * groupWidth);
				float HeightSuwakaOdKrawedzi = (0.07f * groupHeight);
			
				float WitdhSuwaka = (0.88f * groupWidth);
				float HeightSuwaka = (0.88f * groupHeight);
			
				foreach (GameObject atractor in atractors) {
						// 4 - Movement per direction
						atractor.rigidbody2D.velocity = (new Vector2 (
				speed.x * Mathf.Sin (Angle),
				speed.y * Mathf.Cos (Angle)));
				}
				GUI.Box (new Rect (0, 0, groupWidth, groupHeight), "");
				
				

				var change = GUI.VerticalSlider
			(new Rect (WitdhSuwakaOdKrawedzi, HeightSuwakaOdKrawedzi, WitdhSuwaka, HeightSuwaka), InitialAngle, Mathf.PI / scale, -Mathf.PI / scale);
				GUI.EndGroup ();
				if (buttonPressed) {
						Angle += change % Mathf.PI;
						InitialAngle = change;
				}
				this.transform.eulerAngles = new Vector3 (0, 0, Angle * scale);
		}

}
	
	
	

