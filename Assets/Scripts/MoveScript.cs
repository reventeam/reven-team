﻿using UnityEngine;
using System.Collections;

public class MoveScript : MonoBehaviour {

	/// <summary>
	/// 1 - The speed of the ship
	/// </summary>
	public Vector2 speed = new Vector2(10, 10);
	
	// 2 - Store the movement
	private Vector2 movement;

	// Use this for initialization
	void Start () {
	
	}
	
	void Update()
	{
		// 3 - Retrieve axis information
		float inputX = Input.GetAxis("Horizontal");
		float inputY = Input.GetAxis("Vertical");
		
		// 4 - Movement per direction
		movement = new Vector2(
			speed.x * inputX,
			speed.y * inputY);
		
	}
	
	void FixedUpdate()
	{
		rigidbody2D.velocity = movement;
	}
}
