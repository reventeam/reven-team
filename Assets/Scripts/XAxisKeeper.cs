﻿using UnityEngine;
using System.Collections;

public class XAxisKeeper : MonoBehaviour
{
		float yPos;
		// Use this for initialization
		void Start ()
		{
				yPos = this.transform.position.y;
		}
	
		// Update is called once per frame
		void Update ()
		{
				this.camera.transform.position = new Vector3 (this.transform.position.x, yPos, 0);
		}
}
