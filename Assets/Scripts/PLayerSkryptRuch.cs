﻿using UnityEngine;
using System.Collections;

public class PLayerSkryptRuch : MonoBehaviour
{
	
		public Vector2 speed = new Vector2 (50, 50);
		private Vector2 movement;
		private float angle;
	
		void Update ()
		{
				bool lpm = Input.GetMouseButtonDown (0);
				if (lpm) {
						GameObject atraktor = GameObject.FindGameObjectWithTag("Atraktor");
			//atraktor.transform.rigidbody.velocity = new Vector2(0,0);
			var sc = atraktor.GetComponent<GravityPoint>();
			if(sc!=null) sc.enabled = false;
						Object.Instantiate (atraktor,
			                    new Vector3 (
									atraktor.transform.position.x + 2,
									atraktor.transform.position.y + 2,
									0
						),
			                    new Quaternion (0, 0, 0, 0));
			
				}

				// 3 - Retrieve axis information
				angle += Input.GetAxis ("Horizontal") / 10;
		
				// 4 - Movement per direction
				movement = new Vector2 (
			speed.x * Mathf.Sin (angle),
			speed.y * Mathf.Cos (angle));
		
		}

		void FixedUpdate ()
		{
				// 5 - Move the game object
				rigidbody2D.velocity=movement;
		}


}